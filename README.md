# Unlisted versions of Minecraft

## 关于

本仓库整理在启动器中未列出的版本，并提供启动器接口提供下载

这些版本的数据来自 [PrismLauncher/meta](https://github.com/PrismLauncher/meta) 仓库和 [Minecraft Wiki](https://zh.minecraft.wiki/)。

**版权由 Mojang AB 所有。**

## API 接口

```
https://zkitefly.github.io/unlisted-versions-of-minecraft/version_manifest.json
```

格式与 [version_manifest.json](https://zh.minecraft.wiki/w/Version_manifest.json) 保持一致

## 手动安装

前往 `files` 目录中，找到你想下载的版本，下载该版本的 json 文件

打开启动器的 `.minecraft` 目录

进入 `versions` 目录，新建一个文件夹，命名为该 json 的名称

回到启动器刷新版本列表即可找到该版本，启动游戏即可！
